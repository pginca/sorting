#include <stdio.h>
#include <stdlib.h>

/** Size of the vector */
#define TAM_MAX 1000

/** Threshold to change sorting algorithms */
#define K 9

/**
 * @brief This function swap the reference pointer of two integers.
 * @param a a pointer to the first integer
 * @param b a pointer to the second integer
 * @return void
 */
void swap(int *a, int *b){

  int aux = *a;
  *a = *b;
  *b = aux;  
}

/**
 * @brief This function implements the selection sort algorithm.
 * @param V an array
 * @param a index of the first element of V
 * @param b index of the last element of V
 * @return void
 *
 */
void selectionSort(int *V, int a, int b){

  int i, j, min_idx;

  for(i = a; i < b; i++){

    min_idx = i;

    for(j = i + 1; j < b + 1; j++)
      if(V[j] < V[min_idx])
	min_idx = j;

    swap(&V[min_idx], &V[i]);

  }
}

/**
 * @brief This function creates two paritions of the array V based
 *        on a randomly choosen pivot. The resulting array from
 *        this operation is an array where all elements smaller
 *        than the pivot are placed in the left side and all
 *        elements greater than the pivot are placed in right
 *        side of the array.
 * @param V an array
 * @param a index of the first element of V
 * @param b index of the last element of V
 * @param pivot last element of V
 * @return int index of the final position of the pivot in V
 *
 */
int partition(int *V, int a, int b, int pivot){

  int j, i = (a - 1);

  for(j = a; j <= b - 1; j++){
    if(V[j] <= pivot){

      i++;
      swap(&V[i], &V[j]);
    }
  }
  swap(&V[i + 1], &V[b]);
  return (i + 1);  
}

/**
 * @brief This function implement a sorting algorithm that
 *        combines quicksort and selectionsort. It starts executing
 *        the tradicional quicksort while the size of the array
 *        being ordered is greater than a certaing limit. When
 *        the array size is smaller than this limit this sorting
 *        function changes to selectionsort.
 * @param V an array
 * @param a index of the first element of V
 * @param b index of the last element of V
 * @return void
 *
 */
void quickSelect(int *V, int a, int b){

  int pivot;

  /* if the array size is smaller than the threshold,
     then change the sorting procedure to selectionSort */
  if(b - a + 1 < K){
    selectionSort(V, a, b);
    return;
  }

  pivot = partition(V, a, b, V[b]);

  /* recursively call quickSelect for the left and
     right sub arrays */
  quickSelect(V, a, pivot - 1);
  quickSelect(V, pivot + 1, b);

}

void print_array(int *V, int n){

  int i;

  for(i = 0; i < n; i++)
    printf("%d\n", V[i]);

}
 
int main(){

  int V[TAM_MAX], i = 0;

  /* read the input values and compute the number of values */
  while(scanf("%d", &V[i]) == 1)
    i++;

  /* sort the array */
  quickSelect(V, 0, i - 1);

  /* print the array in the stdout */
  print_array(V, i);
  
  return 0;
}
