# INTRODUÇÃO

Ordenação de conjuntos de grandes quantidades de dados
é um problema muito estudado na Ciência da Computação,
seja por suas aplicações práticas ou pelo anseio do aprendizado
teórico. Décadas de estudos geraram conhecimento
sobre o assunto que resultaram em algoritmos de ordenação
muito eficientes. Apesar disso, entender esses algoritmos de
uma maneira profunda, pode não apenas auxiliar a entender
conceitos centrais de projeto e análise de algoritmos, como
também nos faz compreender suas vantagens e desvantagens.
Com esse conhecimento, podemos aplicar o algoritmo que
mais de adapta ao problema a ser resolvido. Seguindo essa
ideia, esse relatório apresenta um algoritmo de ordenação
denominado *QuickSelect*, que é um misto entre os algoritmos
*selectionsort* e *quicksort*, onde a utilização de cada um
depende do tamanho de vetor a ser ordenado. Além disso,
é apresentado uma comparação puramente teórica entre eles.

# ALGORITMOS DE ORDENAÇÃO

## Selectionsort
O algoritmo de ordenação por seleção é um procedimento
eficiente para ordenar arranjos com um número pequeno de
elementos. Este algoritmo mantêm um subarranjo ordenado e
faz uma busca no subarranjo não ordenado para encontrar a
chave com o menor valor, no caso de uma ordenação crescente.
Ao encontrar a chave, o algoritmo faz uma troca de posições
para posicioná-la na posição correta e aumentar o tamanho do
subarranjo já ordenado. O *selectionsort* é conhecido por ser,
apesar de ineficiente para grandes instâncias, um algoritmo
muito simples de ser implementado. Sua complexidade de
tempo é O(n^2). Além disso, ele faz O(n^2) comparações e
O(n) trocas no pior caso, onde n é o número de elementos
no arranjo de entrada. O algoritmo de ordenação
por seleção faz n^2/2 comparações na média.

## Quicksort
O *quicksort* é um algoritmo de ordenação que aplica o
paradigma de divisão e conquista. O procedimento de
divisão, particiona o arranjo em dois subarranjos, onde os
elementos do primeiro subarranjo são menores do que um
elemento chamado pivô, e o elementos do segundo subarranjo
são maiores que o pivô. Já na conquista, os dois subarranjos
são ordenados através de chamadas recursivas ao *quicksort*.
Finalmente, a combinação é trivial, pois como os subarranjos
já estão ordenados, então o arranjo inteiro está ordenado. É
importante ressaltar que a cada vez que o particionamento é
executado, o elemento escolhido como pivô, estará na posição
correta no arranjo ordenado e não será mais trocado de posição
nenhuma vez. A escolha do pivô é uma importante decisão a
ser feita na hora de projetar o *quicksort*, já que essa escolha é
determinante para a eficiência do algoritmo. Uma maneira
bastante comum, e como foi implementado nesse trabalho,
é escolher como pivô o último elemento do arranjo. Esse
algoritmo de ordenação tem complexidade O(n^2) no pior caso.
No entanto, é possível, através da aleatorização da escolha
do pivô, garantir um caso esperado para o quicksort de
complexidade O(n∗lg(n)). O número médio de comparações desse
algoritmo é 1.39*n*lg(n).

# COMPARAÇÃO ENTRE OS ALGORITMOS
Quando temos dois algoritmos é natural o interesse em
fazer uma comparação entre os mesmos, normalmente estamos
interessados em saber qual algoritmo é mais eficiente com
relação à algum recurso, no nosso caso número de comparações.
Existem algumas maneiras de se comparar algoritmos.
A primeira, é implementando e debugando os mesmos. A segunda,
é fazendo uma análise de suas propriedades básicas. A
terceira, é formulando uma hipótese sobre suas performances
comparativas. A quarta opção, é rodando experimento para
validar a hipótese. Esse são métodos científicos aplicados à
análise de algoritmos. Nesse trabalho, vamos nos concentrar
na segunda opção, onde faremos uma comparação teórica
da funções que descrevem o número médio de comparação do
dois algoritmos.
Ao comparar o *selectionsort* e o *quicksort* é fácil perceber
que para instâncias grandes o quicksort tem um desempenho
muito melhor. No entanto, para arranjos de tamanho pequeno
o selectionsort pode ser mais eficiente, pois tem overhead
de recursos menor. Queremos saber qual é o tamanho ótimo,
do ponto de vista teórico, do arranjo para se utilizar cada
um dos algoritmos. Para encontrar o valor ótimo, foi analisado
o crescimento das funções que descrevem o número
de comparações feita por cada um dos algoritmos. Como
podemos ver na Figura 1 o selectionsort faz na média
n^2/2 comparações enquanto o quicksort faz 1.39 *n *lg(n)
comparações. Levando em consideração essas constantes o
ponto de encontro das duas funções é aproximadamente com
um arranjo de tamanho 9. Ou seja, o número de comparações
feitas pelo *selectionsort* é menor para arranjos de tamanho
até aproximadamente 9, após isso o número de comparações
feita pelo *quicksort* é menor.

![alt text](comparacoes.png "Número de comparações feito por cada algoritmo")

# QUICK SELECT
O algoritmo de ordenação *quickselect* é um misto entre
*selectionsort* e *quicksort*. Ao ser chamado a primeira vez e
receber como parâmetro um arranjo com um grande número
de elemento, o *quickselect* executa o *quicksort* normalmente,
particionado o arranjo inteiro em dois subarranjos e fazendo
chamadas recursivas para cada um dos subarranjos. Após
repetidas chamadas recursivas os subarranjos terão um número
de elementos suficientemente pequeno, e o *selectionsort*
passa a ser executado para finalizar a ordenação desses
subarranjos. Nessa implementação, como visto na Seção anterior,
utilizamos subarranjos de tamanho 9 como limiar para a troca
dos algoritmos de ordenação.

# DETALHES DE IMPLEMENTAÇÃO
Para implementar tanto o *quicksort* quanto sua função de
particionamento, foi utilizado como base o pseudo código de
CLRS. Essa é uma implementação recursiva que utliza como pivô
o último elemento do subarranjo de cada chamada recursiva.
Já o *selectionsort* foi implementado de forma iterativa para
minimizar o overhead de fazer novas chamadas recursivas, já
que esse é um algoritmo simples de se implementar iterativamente.
Como o número de elementos que devem ser lidos da
entrada padrão não é conhecido, um vetor é alocado es-
taticamente com um tamanho máximo de 1000 elementos.
Caso uma instância com mais elementos precise ser ordenada,
esse tamanho máximo deve ser ajustado nas diretivas de pré-
processamento do código fonte. O valor do limitar de troca dos
algoritmos pode ser facilmente alterado também nas mesmas
diretivas.

# CONCLUSÃO
O conhecimento a fundo das características, vantagens e
desvantagens dos algoritmos é de fundamental importância
para correto uso dos mesmos. Apesar do conhecimento geral
de que existem algoritmos de ordenação melhores que
outros para grandes instâncias, um conhecimento teórico nos
mostrou que existe um limiar onde a performance é melhor
com diferentes algoritmos para instâncias menores. Nesse
trabalho foi apesentado a análise comparativa dos algoritmos
*selectionsort* e *quicksort* com relação ao seu número de
médio de comparações. Também foi mostrado o ponto ótimo
de troca do *quickselect*.